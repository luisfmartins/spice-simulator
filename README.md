# SPICE Examples

SPICE Circuit Simulator Files for use with *ngspice* simulator

## Example  files
1. ngspice-examples/InvertingAmp_tempo.cir
   Circuit with a Op-Amp in Inverting Amplifier configuration. Time (trasient analysys) 

## Running examples
1. Install ngspice and Gnuplot
2. Run
   ```
   $ cd ngspice-examples
   $ ngspice InvertingAmp_freq.cir`
   ```
3. To produce simulation plots use Gnuplot
   ```
   $ gnuplot -p iampfreq.plt
   ```
   (You may need to change gnuplot terminal type)
